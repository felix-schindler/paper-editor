import { writable, type Writable } from "svelte/store";
import { writable as ls_writable } from "svelte-local-storage-store";

// Settings
export const doc_lang: Writable<"en" | "de" | "zh-Hans"> = ls_writable("doc_lang", "en");

export const figureCount: Writable<number> = writable(0);


export const pages: Writable<Page> = writable();

export type Page = {
};
