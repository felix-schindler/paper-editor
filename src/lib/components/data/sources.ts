import { writable, type Writable } from "svelte/store";

export type Source = {
	author: string;
	url: string;
	lastAccessed: string;
};

export const sources: Writable<Source[]> = writable([
	{
		author: "Felix Schindler",
		url: "https://schindlerfelix.de",
		lastAccessed: "2023-01-05"
	}
]);
